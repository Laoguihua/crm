package com.admiral.crm.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import com.admiral.crm.dao.BaseDictDao;
import com.admiral.crm.domain.BaseDict;

/**
 * �ֵ�Dao��ʵ����
 * @author hasee
 *
 */
public class BaseDictDaoImpl extends HibernateDaoSupport implements BaseDictDao{

	private BaseDictDao baseDictDao;
	
	public void setBaseDictDao(BaseDictDao baseDictDao) {
		this.baseDictDao = baseDictDao;
	}

	@Override
	public List<BaseDict> findByTypeCode(String dict_type_code) {
		return (List<BaseDict>) this.getHibernateTemplate().find("from BaseDict where dict_type_code = ?",dict_type_code);
	}
}
