package com.admiral.crm.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import com.admiral.crm.dao.UserDao;
import com.admiral.crm.domain.User;

public class UserDaoImpl extends HibernateDaoSupport implements UserDao {
	@Override
	public void save(User user) {
		this.getHibernateTemplate().save(user);
	}
	
	@Override
	public User login(User user) {
		List<User> list = (List<User>) this.getHibernateTemplate().find("from User where user_code = ? and user_password = ?", user.getUser_code(),
		user.getUser_password());
		
		
		// 判断内存地址是否为空(有地址不一定有值)， 判断大小是否为空
		if(list!=null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
	
	@Override
	public void regist() {
		// TODO Auto-generated method stub
		
	}
		
	@Override
	public void save() {
		// TODO Auto-generated method stub
		
	}
}