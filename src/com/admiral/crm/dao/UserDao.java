package com.admiral.crm.dao;

import com.admiral.crm.domain.User;

public interface UserDao {
	
	public void regist();
	public void save();
	public void save(User user);
	public User login(User user);

}
