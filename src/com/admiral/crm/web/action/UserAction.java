package com.admiral.crm.web.action;

import org.apache.struts2.ServletActionContext;

import com.admiral.crm.domain.User;
import com.admiral.crm.service.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class UserAction extends ActionSupport implements ModelDriven<User> {
	private User user = new User();
	
	
	/**
	 * 把进来的数据传给user
	 */
	@Override
	public User getModel() {
		return user;
	}
	// 注入 UserService
	private UserService userService;
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	/**
	* 注册的方法:regist
	*/
	public String regist() {
		userService.regist(user);
		return LOGIN;
	}
	
	/**
	* 用户登录的方法
	* Title: login
	* Description:
	* @return
	*/
	public String login() {
		User existUser = userService.login(user);
		if (existUser != null) {
			//登录成功
			//将用户信息保存到 session 中
			//ServletActionContext.getRequest().getSession().setAttribute("existUser",existUser);
			ActionContext.getContext().getSession().put("existUser", existUser);
			return SUCCESS;
		}else {
			//登录失败
			//保存错误信息
			this.addActionError("用户名或密码错误!");
				return LOGIN;
		}
	}
	
}