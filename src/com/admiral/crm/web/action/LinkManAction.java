package com.admiral.crm.web.action;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.admiral.crm.domain.Customer;
import com.admiral.crm.domain.LinkMan;
import com.admiral.crm.domain.PageBean;
import com.admiral.crm.service.CustomerService;
import com.admiral.crm.service.LinkManService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.sun.org.apache.xalan.internal.utils.XMLSecurityManager.Limit;

public class LinkManAction extends ActionSupport implements ModelDriven<LinkMan> {

	//模型驱动使用的对象
	private LinkMan linkMan = new LinkMan();
	
	@Override
	public LinkMan getModel() {
		return linkMan;
	}
	
	
	// 注入 CustomerService
	private CustomerService customerService;
	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	/**
	*
	* Title: saveUI
	* Description: 联系人管理:跳转到添加页面:saveUI
	* @return
	*/
	public String saveUI() {		
		
		// 查询所有客户
		List<Customer> list = customerService.findAll();
		
		// 将所有客户存入值栈
		ActionContext.getContext().getValueStack().set("list",list);
		
		return "saveUI";
	}
	
	/**
	*
	* Title: save
	* Description: 联系人模块:保存联系人的方法
	* @return
	*/
	public String save() {
		linkManService.save(linkMan);
		return "saveSuccess";
	}

	
	//注入 Service
	private LinkManService linkManService;

	public void setLinkManService(LinkManService linkManService) {
		this.linkManService = linkManService;
	}
	
	private Integer currPage = 1;
	private Integer pageSize = 3;

	public void setCurrPage(Integer currPage) {
		if(currPage==null) {
			currPage = 1;
		}
		this.currPage = currPage;
	}
	public void setPageSize(Integer pageSize) {
		if(pageSize==null) {
			pageSize = 3;
		}
		this.pageSize = pageSize;
	}
	
	public String findAll() {
		
		//创建离线条件查询
		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(LinkMan.class);
		
		//设置条件
		if(linkMan.getLkm_name() != null) {
			//设置按名称查询的条件
			detachedCriteria.add(Restrictions.like("lkm_name","%"+
					linkMan.getLkm_name()+"%"));
		}
		
		if(linkMan.getLkm_gender()!=null && !"".equals(linkMan.getLkm_gender())) {
			//设置按性别查询的条件
			detachedCriteria.add(Restrictions.eq("lkm_gender",linkMan.getLkm_gender()));
		}
		
		//调用业务层
		PageBean<LinkMan> pageBean = linkManService.findAll(detachedCriteria,currPage,pageSize);
		
		//将 pageBean 压入值栈中
		ActionContext.getContext().getValueStack().push(pageBean);
		
		System.out.println("开始查询数据，返回列表");
	
		
		return "findAll";
	}
	
	
	/**
	*
	* Title: edit
	* Description: 联系人模块:跳转到编辑页面的方法
	* @return
	*/
	public String edit() {
		
		//查询所有客户
		List<Customer> list = customerService.findAll();
		
		//根据ID查询某个联系人
		linkMan = linkManService.findById(linkMan.getLkm_id());
		
		// 将所有客户和联系人带回到页面
		ActionContext.getContext().getValueStack().set("list",list);
		ActionContext.getContext().getValueStack().push(linkMan);
		
		return "editSuccess";
	}
	
	
	/**
	*
	* Title: update
	* Description: 联系人模块:修改联系人的方法
	* @return
	*/
	public String update() {
		linkManService.update(linkMan);
		return "updateSuccess";
	}
	
	/**
	*
	* Title: delete
	* Description: 联系人模块:删除联系人的方法
	* @return
	*/
	public String delete() {
		//先查询再删除
//		linkMan = linkManService.findById(linkMan.getLkm_id());

		//删除联系人
		linkManService.delete(linkMan);
		
		
		return "saveSuccess";
	}
	
	
	
}



