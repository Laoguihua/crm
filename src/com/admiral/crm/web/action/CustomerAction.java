package com.admiral.crm.web.action;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.admiral.crm.domain.Customer;
import com.admiral.crm.domain.PageBean;
import com.admiral.crm.service.CustomerService;
import com.admiral.crm.utils.UploadUtils;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class CustomerAction extends ActionSupport implements ModelDriven<Customer>{

	// 模型驱动需要用到的对象
	private Customer customer = new Customer();
	
	@Override
	public Customer getModel() {
		return customer;
	}
	
	// 注入 Service
	private CustomerService customerService;
	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	// 注入当前页
	private Integer currPage = 1;
	
	public void setCurrPage(Integer currPage) {
		if (currPage == null) {
			currPage = 1;
		}
		this.currPage = currPage;
	}
	
	// 注入每页记录数
	private Integer pageSize = 3;
	
	public void setPageSize(Integer pageSize) {
		if (pageSize == null) {
			pageSize = 1;
		}
		this.pageSize = pageSize;
	}
	
	/**
	*
	* Title: findAll
	* Description: 客户管理:分页查询
	* @return
	*/
	public String findAll() {
		// 接收分页参数
		DetachedCriteria detacheCriteria = DetachedCriteria.forClass(Customer.class);
		
		// 在web层设置条件
		if(customer.getCust_name() != null) {
			detacheCriteria.add(Restrictions.like("cust_name","%" + 
					customer.getCust_name() + "%"));
		}
		
		if(customer.getBaseDictSource() != null) {
			if(customer.getBaseDictSource().getDict_id() != null 
					&& !"".equals(customer.getBaseDictSource().getDict_id())) {
				detacheCriteria.add(Restrictions.eq("baseDictSource.dict_id",
						customer.getBaseDictSource().getDict_id()));
			}
		}
		
		if(customer.getBaseDictLevel() != null) {
			if(customer.getBaseDictLevel().getDict_id() != null 
					&& !"".equals(customer.getBaseDictLevel().getDict_id())) {
				detacheCriteria.add(Restrictions.eq("baseDictLevel.dict_id",
						customer.getBaseDictLevel().getDict_id()));
			}
		}
		
		if(customer.getBaseDictIndustry() != null) {
			if(customer.getBaseDictIndustry().getDict_id() != null 
					&& !"".equals(customer.getBaseDictIndustry().getDict_id())) {
				detacheCriteria.add(Restrictions.eq("baseDictIndustry.dict_id",
						customer.getBaseDictIndustry().getDict_id()));
			}
		}	
		
		System.out.println(pageSize);
		
		//调用业务层查询
		PageBean<Customer> pageBean = customerService.findByPage(detacheCriteria,currPage,pageSize);
		
		//将分页对象保存到值栈
		ActionContext.getContext().getValueStack().push(pageBean);
		
		return "findAll";
	}
	
	
	
	/**
	*
	* Title: saveUI
	* Description: 客户管理:跳转到添加页面:saveUI
	* @return
	*/
	public String saveUI() {
		return "saveUI";
	}

	private String uploadFileName;
	private File upload;
	private String uploadContextType;
	
	/**
	*
	* Title: save
	* Description: 客户管理:保存客户
	* @return
	 * @throws IOException 
	*/
	public String save() throws IOException {
		
		if(upload != null) {
			//上传资质图片
			//设置文件上传的路径
			String path = "K:/启迪实训/第11天-crm/upload";
			
			// 一个目录下存放的相同文件名:文件名随机
			String uuidFileName = UploadUtils.getUuidFileName(uploadFileName);
			
			// 一个目录下存放的文件过多:目录分离
			String realPath = UploadUtils.getPath(uuidFileName);
			
			//创建目录
			String url = path + realPath;
			File file = new File(url);
			if(!file.exists()) {
				//创建多级目录
				file.mkdirs();
			}
			
			//进行文件上传
			File destFile = new File(url + "/" + uuidFileName);
			FileUtils.copyFile(upload, destFile);
			
			customer.setCust_image(url + "/" + uuidFileName);
		}
		
		customerService.save(customer);
		return "saveSuccess";
	}
	
	/**
	 * Descrption: 删除客户的方法
	 * @return
	 */
	public String delete() {
		
		//先查询再删除
		customer = customerService.findById(customer.getCust_id());
		
		if(customer.getCust_image() != null) {
			File file = new File(customer.getCust_image());
			if(file != null) {
				file.delete();
			}
		}
		
		customerService.delete(customer);
		
		return "deleteSuccess";
	}

	/**
	 * 
	 * @return
	 */
	public String edit() {
		// 根据 ID 查询,跳转页面,回显数据
		customer = customerService.findById(customer.getCust_id());
		
		// 将 customer 传递到页面
		// 两种方式:第一种:手动压栈. 第二种:因为模型驱动的对象,默认就在栈顶
		// 如果使用第一种方式:回显数据: <s:property value="cust_name"/>
		// 如果使用第二种方式:回显数据: <s:property value="model.custo_name"/>
		return "editSuccess";
	}
	
	public String update() throws IOException {
		
		//判断文件项是否已经选择,
		//如果选择了,删除原有文件,上传新文件,如果没有选择,使用原有文件即可
		if(upload!=null) {
			//已经选择了
			//删除原有文件
			
			String cust_image = customer.getCust_image();//从隐藏域中获取原有路径
			if(cust_image!=null || "".equals(cust_image)) {
				File file = new File(cust_image) ;
				if(file != null) {
					file.delete();
				}
			}
			
			//文件上传:
			//设置文件上传的路径
			String path = "K:/启迪实训/第11天-crm/upload";

			// 一个目录下存放的相同文件名:文件名随机
			String uuidFileName = UploadUtils.getUuidFileName(uploadFileName);
			
			// 一个目录下存放的文件过多:目录分离
			String realPath = UploadUtils.getPath(uuidFileName);
			
			//创建目录
			String url = path + realPath;
			File file = new File(url);
			if(!file.exists()) {
				//创建多级目录
				file.mkdirs();
			}
			
			//进行文件上传
			File destFile = new File(url + "/" + uuidFileName);
			FileUtils.copyFile(upload,destFile);
			
			//设置文件上传的路径
			customer.setCust_image(url + "/" + uuidFileName);
			
		}
		customerService.update(customer);
		
		return "updateSuccess";

	}
	
	


	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadContextType() {
		return uploadContextType;
	}

	public void setUploadContextType(String uploadContextType) {
		this.uploadContextType = uploadContextType;
	}
	
	
	
	
	
}















