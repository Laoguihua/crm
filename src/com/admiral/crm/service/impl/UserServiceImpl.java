package com.admiral.crm.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.admiral.crm.dao.UserDao;
import com.admiral.crm.domain.User;
import com.admiral.crm.service.UserService;
import com.admiral.crm.utils.MD5Utils;

@Transactional
public class UserServiceImpl implements UserService {
	// 注入 UserDao
	private UserDao userDao;
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	@Override
	public void regist(User user) {
		//对密码进行加密处理
		user.setUser_password(MD5Utils.md5(user.getUser_password()));
		//设置用户状态
		user.setUser_state("1");
		userDao.save(user);
	}
	@Override
	public User login(User user) {
		//对密码进行加密
		user.setUser_password(MD5Utils.md5(user.getUser_password()));
		return userDao.login(user);
	}
}