package com.admiral.crm.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.transaction.annotation.Transactional;

import com.admiral.crm.dao.CustomerDao;
import com.admiral.crm.domain.Customer;
import com.admiral.crm.domain.PageBean;
import com.admiral.crm.service.CustomerService;

@Transactional
public class CustomerServiceImpl implements CustomerService{

	// 注入客户的 Dao
	private CustomerDao customerDao;
	
	public void setCustomerDao(CustomerDao customerDao) {
		this.customerDao = customerDao;
	}

	@Override
	public void save(Customer customer) {
		customerDao.save(customer);
	}

	@Override
	public PageBean<Customer> findByPage(DetachedCriteria detachedCriteria, Integer currPage, Integer pageSize) {

		PageBean<Customer> pageBean = new PageBean<Customer>();
		
		//封装当前页
		pageBean.setCurrPage(currPage);
		//封装每页记录数
		pageBean.setPageSize(pageSize);
		
		//封装总记录数:查询
		//调用Dao
		Integer totalCount = customerDao.findCount(detachedCriteria);
		pageBean.setTotalCount(totalCount);
		//封装总页数:计算
		Double num = Math.ceil(totalCount.doubleValue()/pageSize);
		pageBean.setTotalPage(num.intValue());

//		System.out.println("currPage================="+currPage+",pageSize================="+pageSize);
		
		//封装每页显示的数据的集合
		Integer begin = (currPage - 1)*pageSize;
		
//		System.out.println("begin================="+begin);
		
		List<Customer> customers = customerDao.findByPage(detachedCriteria,begin,pageSize);
		
		pageBean.setList(customers);
		
		
		return pageBean;
	}

	@Override
	public Customer findById(Long cust_id) {
		return customerDao.findById(cust_id);
	}

	@Override
	public void delete(Customer customer) {
		customerDao.delete(customer);
	}

	@Override
	public void update(Customer customer) {
		customerDao.update(customer);
		
	}

	@Override
	public List<Customer> findAll() {
		return customerDao.findAll();
	}

	
	
	
	
}
