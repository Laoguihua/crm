package com.admiral.crm.service.impl;

import java.util.List;

import com.admiral.crm.dao.BaseDictDao;
import com.admiral.crm.domain.BaseDict;
import com.admiral.crm.service.BaseDictService;

public class BaseDictServiceImpl implements BaseDictService{

	private BaseDictDao baseDictDao;
	
	public void setBaseDictDao(BaseDictDao baseDictDao) {
		this.baseDictDao = baseDictDao;
	}
	
	public List<BaseDict> findByTypeCode(String dict_type_code){
		
		return baseDictDao.findByTypeCode(dict_type_code);
	}
	
}
