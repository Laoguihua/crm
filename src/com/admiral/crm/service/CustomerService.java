package com.admiral.crm.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.admiral.crm.domain.Customer;
import com.admiral.crm.domain.PageBean;

public interface CustomerService {

	void save(Customer customer);

	PageBean<Customer> findByPage(DetachedCriteria detacheCriteria, Integer currPage, Integer pageSize);

	Customer findById(Long cust_id);

	void delete(Customer customer);

	void update(Customer customer);

	List<Customer> findAll();

}
