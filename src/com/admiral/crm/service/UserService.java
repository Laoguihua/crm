package com.admiral.crm.service;

import com.admiral.crm.domain.User;

public interface UserService {

	void regist(User user);

	User login(User user);

}
