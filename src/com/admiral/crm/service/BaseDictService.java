package com.admiral.crm.service;

import java.util.List;

import com.admiral.crm.dao.BaseDictDao;
import com.admiral.crm.domain.BaseDict;


public interface BaseDictService {

	List<BaseDict> findByTypeCode(String dict_type_code);;

}
