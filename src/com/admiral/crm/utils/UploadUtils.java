package com.admiral.crm.utils;

import java.util.UUID;

/**
 * 
 * @Description: 文件上传的工具类
 * @author Admiral
 */
public class UploadUtils {
	
	/**
	 * 
	 * Title: getUuidFileName 
	 * Description: 解决目录下文件名重复的问题
	 * @param fileName :文件名
	 * @return : 随机产生的唯一文件名
	 */
	public static String getUuidFileName(String fileName) {
		int indexOf = fileName.lastIndexOf(".");
		String extions = fileName.substring(indexOf);
		return UUID.randomUUID().toString().replace("-", "") + extions;
		
	}
	
	public static String getPath(String uuidFileName) {
		int code1 = uuidFileName.hashCode();
		int d1 = code1 & 0xf;		//作为1级目录
		int code2 = code1 >>> 4;
		int d2 = code2 & 0xf;		//作为2级目录
		return "/" + d1 + "/" + d2;
	}
	
	public static void main(String[] args) {
		System.out.println(UploadUtils.getUuidFileName("aa.txt"));
	}
}



