package com.admiral.crm.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.admiral.crm.domain.Customer;

/**
 * 客户管理的Dao接口
 */
public interface CustomerDao {

	/**
	 * 
	 * Title: save 
	 * Description: 客户管理:Dao层保存客户的方法
	 * @param customer
	 */
	public void save(Customer customer);

	/**
	 * 
	 * Title: findCount 
	 * Description: 客户管理:Dao层带条件查询记录数
	 * @param detachedCriteria
	 * @return
	 */
	public Integer findCount(DetachedCriteria detachedCriteria);

	/**
	 * 
	 * Title: findByPage 
	 * Description: 客户管理:Dao层待条件分页查询
	 * @param detachedCriteria :分页查询的条件
	 * @param begin :分页查询的开始索引
	 * @param pageSize :分页查询每页显示的记录数
	 * @return
	 */
	public List<Customer> findByPage(DetachedCriteria detachedCriteria, Integer begin, Integer pageSize);

	/**
	 * 
	 * Title: findById 
	 * Description: 客户管理:Dao层根据ID查询客户信息
	 * @param cust_id : 要查询的客户的ID
	 * @return : 查询到的客户信息
	 */
	public Customer findById(Long cust_id);

	/**
	 * 
	 * Title: delete 
	 * Description: 客户管理:Dao层删除客户的方法
	 * @param customer
	 */
	public void delete(Customer customer);

	/**
	 * 
	 * Title: update 
	 * Description: 客户管理:Dao层修改客户的方法
	 * @param customer
	 */
	public void update(Customer customer);

}
