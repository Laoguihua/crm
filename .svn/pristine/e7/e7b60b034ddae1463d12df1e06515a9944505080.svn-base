package com.admiral.crm.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.transaction.annotation.Transactional;

import com.admiral.crm.dao.LinkManDao;
import com.admiral.crm.domain.LinkMan;
import com.admiral.crm.domain.PageBean;
import com.admiral.crm.service.LinkManService;

/**
 * 
 * @Description: 联系人Service层的实现类
 * @author Admiral
 */
@Transactional
public class LinkManServiceImpl implements LinkManService {

	//注入 Dao
	private LinkManDao linkManDao;

	public void setLinkManDao(LinkManDao linkManDao) {
		this.linkManDao = linkManDao;
	}

	@Override
	public PageBean<LinkMan> findAll(DetachedCriteria detachedCriteria, Integer currPage, Integer pageSize) {
		PageBean<LinkMan> pageBean = new PageBean<LinkMan>();
		//设置当前页
		pageBean.setCurrPage(currPage);
		//设置每页的记录数
		pageBean.setPageSize(pageSize);
		
		//设置总记录数
		Integer totalCount = linkManDao.findCount(detachedCriteria);
		pageBean.setTotalCount(totalCount);
		
		//设置总页数
		Double num = Math.ceil(totalCount.doubleValue()/pageSize);
		pageBean.setTotalPage(num.intValue());
		
		//设置每页显示的数据
		Integer begin = (currPage-1) * pageSize;
		List<LinkMan> linkMans = linkManDao.findByPage(detachedCriteria,begin,pageSize);
		pageBean.setList(linkMans);
		
		return pageBean;
	}

	@Override
	public void save(LinkMan linkMan) {
		linkManDao.save(linkMan);
	}
	
}
